import { Router } from 'express';
import EmployeeController from '../controllers/employee.controller';
import EmployeeRepository from '../repositories/employee.repository';
import EmployeeService from '../services/employee.service';

const employeeRepository = new EmployeeRepository();
const employeeService = new EmployeeService(employeeRepository);
const employeeController = new EmployeeController(employeeService);

const router = Router();

router.get('/', employeeController.getAllEmployees());
router.post('/', employeeController.saveNewEmployee());
router.put('/:employeeId/', employeeController.updateEmployee());
router.delete('/:employeeId/', employeeController.deleteEmployee());

export default router;
