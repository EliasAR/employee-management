import { Router } from 'express';

import employees from './employee.routes';

const router = Router();
const path = '/api/v1/';

router.use(`${path}employees`, employees);

// Not Found
router.use((req, res) => {
  res.status(404).json({ message: 'Endpoint no encontrado. :(', data: null });
});

export default router;
