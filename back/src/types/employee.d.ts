export interface IEmployee {
  id: number;
  last_name: string;
  sur_name: string;
  first_name: string;
  other_name: string;
  type_id: string;
  identification: string;
  email: string;
  country: string;
  area: string;
  start_date: string;
  status: string;
  create_date: string;
  change_date: string;
}

type EmployeeDTO = Omit<IEmployee, 'id,email,status,create_date'>;
