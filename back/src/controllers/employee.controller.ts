import { Request, Response } from 'express';
import EmployeeService from '../services/employee.service';
import ResponseDto from '../types/responseDto';
import Controller from './controller';

export default class EmployeeController extends Controller {
  employeeService: EmployeeService;
  constructor(employeeService: EmployeeService) {
    super();
    this.employeeService = employeeService;
  }

  getAllEmployees = () => async (req: Request, res: Response): Promise<void> => {
    let response: ResponseDto;
    try {
      const employees = this.employeeService.getAllEmployees();
      response = this.customResponse(200, 'Empleados', employees);
    } catch (error) {
      if (error instanceof Error) response = this.badResponse(error.message);
      else response = this.failResponse(error);
    }
    res.status(response.code).json(response);
  };

  saveNewEmployee = () => async (req: Request, res: Response): Promise<void> => {
    res.header('Access-Control-Allow-Origin', '*');
    let response: ResponseDto;
    try {
      const employee = this.employeeService.addNewEmployee(req.body);
      response = this.customResponse(201, 'Empleado nuevo', employee);
    } catch (error) {
      if (error instanceof Error) response = this.badResponse(error.message);
      else response = this.failResponse(error);
    }
    res.status(response.code).json(response);
  };

  updateEmployee = () => async (req: Request, res: Response): Promise<void> => {
    let response: ResponseDto;
    try {
      this.employeeService.updateEmployee(Number(req.params.employeeId), req.body);
      response = this.customResponse(200, 'Empleado actualizado', null);
    } catch (error) {
      if (error instanceof Error) response = this.badResponse(error.message);
      else response = this.failResponse(error);
    }
    res.status(response.code).json(response);
  };

  deleteEmployee = () => async (req: Request, res: Response): Promise<void> => {
    let response: ResponseDto;
    try {
      this.employeeService.deleteEmployee(Number(req.params.employeeId));
      response = this.customResponse(200, 'Empleado eliminado', null);
    } catch (error) {
      if (error instanceof Error) response = this.badResponse(error.message);
      else response = this.failResponse(error);
    }
    res.status(response.code).json(response);
  };
}
