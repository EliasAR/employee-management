import moment from 'moment';
import EmployeeRepository from '../repositories/employee.repository';
import { EmployeeDTO, IEmployee } from '../types/employee';

export default class EmployeeService {
  employeeRepository: EmployeeRepository;

  constructor(employeeRepository: EmployeeRepository) {
    this.employeeRepository = employeeRepository;
  }

  getAllEmployees(): IEmployee[] {
    const users = this.employeeRepository.getAll();
    return users;
  }

  addNewEmployee(employee: EmployeeDTO): IEmployee {
    const newEmployee: IEmployee = {
      ...employee,
      status: 'Activo',
      email: this.generateEmployeeMail(employee),
      start_date: moment(employee.start_date).format('YYYY-MM-DD'),
      create_date: moment().format('YYYY-MM-DD HH:mm:ss'),
    };
    const users = this.employeeRepository.addNew(newEmployee);
    return users;
  }

  updateEmployee(employeeId: number, employee: IEmployee): void {
    employee.id = employeeId;
    employee.change_date = moment().format('YYYY-MM-DD HH:mm:ss');
    employee.email = this.generateEmployeeMail(employee);

    this.employeeRepository.updateOne(employee);
  }

  deleteEmployee(employeeId: number): void {
    this.employeeRepository.deleteOne(employeeId);
  }

  private generateEmployeeMail(employee: EmployeeDTO): string {
    const userName = `${employee.first_name.replace(/\s+/g, '')}.${employee.last_name.replace(/\s+/g, '')}`;
    let email = `${userName}@cidenet.com.co`;

    const id = this.employeeRepository.validateEmail(employee.id, email);

    if (id != null) email = `${userName}.${id}@cidenet.com.co`;

    return email;
  }
}
