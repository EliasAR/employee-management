import fs from 'fs';
import { IEmployee } from '../types/employee';

const jsonEmployess = fs.readFileSync('src/db/employees.json', 'utf-8');
const employees: IEmployee[] = JSON.parse(jsonEmployess);

export default class EmployeeRepository {
  getAll(): IEmployee[] {
    return employees;
  }

  validateEmail(employeeId: number | undefined, email: string): number | null {
    const employee = employees.find((x) => x.email === email && (!employeeId || employeeId !== x.id));
    if (employee) return Math.max(...employees.map((x) => x.id)) + 1;
    else return null;
  }

  addNew(newEmployee: IEmployee): IEmployee {
    const newId = Math.max(...employees.map((x) => x.id)) + 1;
    newEmployee.id = newId > 0 ? newId : 1;
    employees.push(newEmployee);
    this.updateDB();
    return newEmployee;
  }

  updateOne(employee: IEmployee): void {
    const index = employees.findIndex((x) => x.id === employee.id);
    if (index >= 0) employees[index] = employee;
    else throw new Error('No se encuentra el usuario');
    this.updateDB();
  }

  deleteOne(employeeId: number): void {
    const index = employees.findIndex((x) => x.id === employeeId);
    if (index >= 0) employees.splice(index, 1);
    else throw new Error('No se encuentra el usuario');
    this.updateDB();
  }

  private updateDB(): void {
    fs.writeFileSync('src/db/employees.json', JSON.stringify(employees), 'utf-8');
  }
}
